import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs';
import { Post } from './post';
import { User } from './user';
import { Comment } from './comment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.apiUrl + '/posts');
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + '/users');
  }

  getUserInfo(userId): Observable<User> {
    return this.http.get<User>(this.apiUrl + '/users/' + userId);
  }

  getComments(postId): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.apiUrl + '/comments?postId=' + postId);
  }

  performSearch(userIds, postTitle: string): Observable<any> {
    const requests: Observable<Post[]>[] = [];

    if (userIds && userIds.length > 0) {
      for (const userId of userIds) {
        const apiEnd =
          '/posts?userId=' +
          userId +
          (postTitle !== '' ? '&title=' + postTitle : '');

        requests.push(this.http.get<Post[]>(this.apiUrl + apiEnd));
      }
    } else if (postTitle) {
      requests.push(
        this.http.get<Post[]>(this.apiUrl + '/posts?title=' + postTitle)
      );
    } else {
      requests.push(this.http.get<Post[]>(this.apiUrl + '/posts'));
    }

    return forkJoin(requests);
  }
}
