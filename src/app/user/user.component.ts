import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { User } from './../user';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  userInfo: User = {
    id: null,
    name: '',
    username: '',
    email: '',
    address: {
      street: '',
      city: '',
      suite: '',
      zipcode: '',
      geo: { lat: '', lng: '' },
    },
    phone: '',
    website: '',
    company: '',
  };
  userId: number;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.userId = +this.route.snapshot.paramMap.get('id');
    this.getUserInfo(this.userId);
  }

  getUserInfo(userId): void {
    this.apiService
      .getUserInfo(userId)
      .pipe(take(1))
      .subscribe((results) => (this.userInfo = results));
  }
}
