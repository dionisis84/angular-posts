import { Post } from './../post';
import { ApiService } from './../api.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  @Input() users: SelectItem[];
  @Input() queryUser;
  selectedUsers = [];
  titleSearch = '';
  @Output() fetchedPosts = new EventEmitter<Post[]>();

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    if (this.queryUser > 0) {
      this.selectedUsers.push(this.queryUser);
      this.doSearch();
    }
  }

  onSearch(): void {
    this.doSearch();
  }

  onClear(): void {
    this.selectedUsers = [];
    this.titleSearch = '';
    this.fetchedPosts.emit(null);
  }

  doSearch(): void {
    this.apiService
      .performSearch(this.selectedUsers, this.titleSearch)
      .pipe(take(1))
      .subscribe((results) => {
        let searchResult = [];

        for (const result of results) {
          searchResult = searchResult.concat(result);
        }

        this.fetchedPosts.emit(searchResult);
      });
  }
}
