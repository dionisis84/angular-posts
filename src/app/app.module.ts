import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { UserComponent } from './user/user.component';
import { CardModule } from 'primeng/card';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { SearchComponent } from './search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { PostsTableComponent } from './posts-table/posts-table.component';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { CommentsTableComponent } from './comments-table/comments-table.component';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    UserComponent,
    SearchComponent,
    PostsTableComponent,
    DialogComponent,
    CommentsTableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CardModule,
    MultiSelectModule,
    FormsModule,
    BrowserAnimationsModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    HttpClientModule,
    ContextMenuModule,
    DialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
