import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Post } from './../post';
import { Comment } from './../comment';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  users: SelectItem[];
  posts: Post[] = null;
  displayDialog = false;
  postsCols: any[];
  postComments: Comment[] = null;
  userQuery;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.postsCols = [
      { field: 'id', header: 'ID' },
      { field: 'userName', header: 'User' },
      { field: 'title', header: 'Title' },
      { field: 'body', header: 'Body' },
    ];

    this.userQuery = +this.route.snapshot.queryParamMap.get('uid');

    this.getInfo();
  }

  // Get all the information needed from the API
  getInfo(): void {
    this.apiService
      .getUsers()
      .pipe(take(1))
      .subscribe(
        (results) =>
          (this.users = results.map((user) => ({
            label: user.name,
            value: user.id,
          })))
      );
  }

  postsFetched(posts): void {
    if (posts !== null) {
      // Set users names in results
      this.posts = posts.map((post) => {
        post.userName = this.users.filter(
          (user) => user.value === post.userId
        )[0].label;

        return post;
      });
    } else {
      this.posts = posts;
    }
  }

  toggleDialog(postId): void {
    this.apiService
      .getComments(postId)
      .pipe(take(1))
      .subscribe((results) => {
        this.postComments = results;
        this.displayDialog = !this.displayDialog;
      });
  }
}
