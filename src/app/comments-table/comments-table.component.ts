import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-comments-table',
  templateUrl: './comments-table.component.html',
  styleUrls: ['./comments-table.component.css'],
})
export class CommentsTableComponent implements OnInit {
  @Input() content: any[];
  @Input() cols: any[];

  constructor() {}

  ngOnInit(): void {}
}
