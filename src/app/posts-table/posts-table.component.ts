import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-posts-table',
  templateUrl: './posts-table.component.html',
  styleUrls: ['./posts-table.component.css'],
})
export class PostsTableComponent implements OnInit {
  @Input() content: any[];
  @Input() cols: any[];
  @Input() enableContextMenu: boolean;
  selectedContent: any;
  items: MenuItem[];
  @Output() selectedContentId = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {
    this.items = [
      {
        label: 'View Comments',
        icon: 'pi pi-eye',
        command: (event) =>
          this.selectedContentId.emit(this.selectedContent.id),
      },
    ];
  }
}
