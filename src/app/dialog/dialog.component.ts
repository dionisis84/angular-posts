import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent implements OnInit {
  @Input() display: boolean;
  @Input() comments;
  @Input() users;
  commentsCols: any[];
  @Output() displayChange = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    this.commentsCols = [
      { field: 'id', header: 'ID' },
      { field: 'name', header: 'Name' },
      { field: 'email', header: 'Email' },
      { field: 'body', header: 'Body' },
    ];
  }

  onHideDialog(): void {
    this.displayChange.emit(this.display);
  }
}
